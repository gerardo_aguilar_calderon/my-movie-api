class NotFoundException(Exception):
    """
    Class to define an error when an object is not found in the database.
    """
    def __init__(self, *args: object) -> None:
        super().__init__(*args)