import typing
from fastapi import FastAPI
from fastapi.responses import JSONResponse
from starlette.middleware.base import BaseHTTPMiddleware, RequestResponseEndpoint
from starlette.requests import Request
from starlette.responses import Response

from exceptions.not_found import NotFoundException

class ErrorHandler(BaseHTTPMiddleware):
    """
    General error handler.
    """
    def __init__(self, app: FastAPI) -> None:
        super().__init__(app)


    async def dispatch(self, request: Request, call_next: RequestResponseEndpoint) -> Response | JSONResponse:
        try:
            return await call_next(request)
        except NotFoundException as e:
            return JSONResponse(status_code=404, content={"message": str(e)})
        except Exception as e:
            return JSONResponse(status_code=500, content={"message": str(e)})