from typing import Any, Coroutine
from fastapi import HTTPException, Request
from fastapi.security import HTTPAuthorizationCredentials, HTTPBearer

from jwt_manager import validate_token


class JWTBearer(HTTPBearer):    
    async def __call__(self, request: Request) -> Coroutine[Any, Any, HTTPAuthorizationCredentials | None]:
        auth = await super().__call__(request)
        token_data = validate_token(auth.credentials)
        if token_data["email"] != "gerardo.aguilar.calderon@gmail.com":
            raise HTTPException(status_code=403, detail="Invalid credentials")