import os
from sqlalchemy import create_engine
from sqlalchemy.orm.session import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

# file name for sqlite database.
sqlite_file_name = "../database.sqlite"

# base directory for current file.
base_dir = os.path.dirname(os.path.realpath(__file__))

# database url.
database_url = f"sqlite:///{os.path.join(base_dir, sqlite_file_name)}"

# engine for database.
engine = create_engine(database_url, echo=True)

# session connection for database.
Session = sessionmaker(bind=engine)

# base for database.
Base = declarative_base()