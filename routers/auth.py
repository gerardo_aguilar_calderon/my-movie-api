from fastapi import APIRouter
from fastapi.responses import JSONResponse
from jwt_manager import create_token

from models.dto import User

auth_router = APIRouter()

@auth_router.post('/login', tags=['auth'])
def login(user: User):
    if user.email == "gerardo.aguilar.calderon@gmail.com" and user.password == "admin":
        token = create_token(user.model_dump())
        return JSONResponse(status_code=200, content=token)
    return JSONResponse(status_code=404, content={"message": "User or password incorrect."})