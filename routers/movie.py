from fastapi import APIRouter, Path, Query
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse
from config.database import Session

from models.dto import CreateMovieDto, UpdateMovieDto
from services.movie import MovieService

movie_router = APIRouter()

@movie_router.get('/movies', tags=['movies'])
def get_movies():
    db = Session()
    result = MovieService(db).get_movies()
    return JSONResponse(status_code=200, content=jsonable_encoder(result))


@movie_router.get('/movies/{id}', tags=['movies'])
def get_movie_by_id(id: int = Path(ge=1)):
    db = Session()
    result = MovieService(db).get_movie_by_id(id)
    return JSONResponse(status_code=200, content=jsonable_encoder(result))


@movie_router.get('/movies/', tags=['movies'])
def get_movies_by_category(category: str = Query(min_length=5, max_length=15)):
    db = Session()
    result = MovieService(db).get_movie_by_category(category=category)
    return JSONResponse(status_code=200, content=jsonable_encoder(result))


@movie_router.post('/movies', tags=['movies'])
async def create_movie(movie: CreateMovieDto):
    """
    Endpoint to create a new movie.
    * movie - CreateMovieDto object to execute process.
    """
    db = Session()
    movie_created = MovieService(db).create_movie(dto=movie)
    return JSONResponse(status_code=201, content={"message": "Movie created."})


@movie_router.put('/movies/{id}', tags=['movies'])
async def update_movie(updateMovieDto: UpdateMovieDto, id: int = Path(ge=1)):
    db = Session()
    MovieService(db).update_movie(id=id, dto=updateMovieDto)
    return JSONResponse(status_code=200, content={"message": "Movie updated."})


@movie_router.delete('/movies/{id}', tags=['movies'])
def delete_movie(id: int):
    db = Session()
    MovieService(db).delete_movie(id)
    return JSONResponse(status_code=200, content={"message": "Movie deleted."})
