from config.database import Session
from exceptions.not_found import NotFoundException
from models.dto import CreateMovieDto, UpdateMovieDto
from models.movie import Movie


class MovieService():

    def __init__(self, db: Session) -> None:
        self.db = db


    def get_movies(self) -> list[Movie]:
        """
        Retrieves all the movies from db.
        """
        return self.db.query(Movie).all()
    

    def get_movie_by_id(self, id: int) -> Movie:
        """
        Retrieves a movie given a numeric id.

        Raises NotFoundException if the movie cannot be found.
        """
        movie_found = self.db.query(Movie).filter(Movie.id == id).first()

        if not movie_found:
            raise NotFoundException('Movie not found.')
        
        return movie_found
    

    def get_movie_by_category(self, category: str) -> list[Movie]:
        """
        Retrieves a list of movies that matches with the given category.

        category -> Category value for movie filtering.
        """
        return self.db.query(Movie).filter(Movie.category == category).all()
    

    def create_movie(self, dto: CreateMovieDto) -> None:
        """
        Creates a new movie in the database.

        dto -> Object with the required data to create a new movie.
        """
        movie_created = Movie(**dto.model_dump())
        self.db.add(movie_created)
        self.db.commit()

    
    def update_movie(self, id: int, dto: UpdateMovieDto) -> Movie:
        """
        Updates a movie from the database given an id.

        Raises NotFoundException if the movie cannot be found.
        """
        movie_found = self.db.query(Movie).filter(Movie.id == id).first()

        if not movie_found:
            raise NotFoundException('Movie not found.')
        
        movie_found.title = dto.title
        movie_found.overview = dto.overview
        movie_found.year = dto.year
        movie_found.rating = dto.rating
        movie_found.category = dto.category
        self.db.commit()

    
    def delete_movie(self, id: int) -> None:
        """
        Deletes a movie from the database given an id.

        Raises NotFoundException if the movie cannot be found.
        """
        movie_found = self.db.query(Movie).filter(Movie.id == id).first()

        if not movie_found:
            raise NotFoundException('Movie not found.')
        
        self.db.delete(movie_found)
        self.db.commit()