from pydantic import BaseModel, Field

class User(BaseModel):
    """
    User model to generate JWTs.
    """
    email: str
    password: str


class CreateMovieDto(BaseModel):
    """
    DTO model to represent request for creating a new movie
    """
    id: int | None = None
    title: str = Field(min_length=5)
    overview: str = Field(min_length=15)
    year: int = Field(le=2023)
    rating: float = Field(le=10.0)
    category: str = Field(min_length=6, max_length=20)

    class Config:
        json_schema_extra = {
            "example": {
                "id": 1,
                "title": "Mi película",
                "overview": "Descripción de la película",
                "year": 2023,
                "rating": 8.5,
                "category": "Comedia"
            }
        }


class UpdateMovieDto(BaseModel):
    """
    DTO model to represent data for movie updating.
    #### Attributes:
    - title: Title value for movie.
    - overview: Overview value for movie.
    - year: Year value for movie.
    - rating: Rating value for movie.
    - category: Category value for movie.
    """
    title: str = Field(min_length=5)
    overview: str = Field(min_length=15)
    year: int = Field(le=2023)
    rating: float = Field(le=10.0)
    category: str = Field(min_length=6, max_length=20)

    class Config:
        json_schema_extra = {
            "example": {
                "title": "Mi película",
                "overview": "Descripción de la película",
                "year": 2023,
                "rating": 8.5,
                "category": "Comedia"
            }
        }
